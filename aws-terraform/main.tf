provider "aws" {
  version = "2.69.0"
  region="ap-southeast-1"
}

      # - title: Ubuntu22.04
      #   const: ami-0a72af05d27b49ccb
      # - title: CentOS9
      #   const: ami-07f1e6b2a73037f9b

variable "instance_type" {
  description = "AWS instance type"
  default = "t1.micro"
}
variable "ami" {
  description = "AWS Image ID"
  default = "ami-0a72af05d27b49ccb"
}

variable "myTag" {
  description = "My Input Tag"
  default = "terraform-test"
}

resource "aws_instance" "machine1" {
  ami = var.ami
  instance_type = var.instance_type
  availability_zone = "ap-southeast-1a"
  key_name = "joey-new"
  tags = {
      "type" = var.myTag
      "name" = "machine1"
    }
}

# resource "aws_instance" "machine2" {
# ami = var.ami
# instance_type = var.instance_type
# availability_zone = "ap-southeast-1a"
# tags = {
#     "type" = var.myTag
#     "name" = "machine2"
#     }
# }

# resource "aws_key_pair" "deployer" {
#   key_name   = "deployer-key"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDl3T/iVn65arUw+D+axLGvTnf/nTRIpxt4SLGNwnSZW3SCYJJQwxRxnGbqg0Pe3DArP0ohrH4D58RZBHQM0C3nKuUtk486/uTaHMyO++WqUL79c388HYHIyVD/me6NgrMVN/v1DUI1qNLEmHLd00ho/Z+2uBDe8F3OIRjFHEN1LCX0Hgkgd6/Y6ivpiX7SOniXYq+P1zNGiCzIY1Pt0ddI6aD0uwhKTHgbiBH6BgG/lk3K7cdUkSq/e4j88kl5N4n7k7obmIE0GHBOsUyF3hQvcjQ2DLaAjbygIrA8E1LwHQk9TmXPEUtdQYfBXqjVayq2+CsxUqTc89YNAcSyIFUv root@Master01"
# }
